﻿using System.IO;
using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    internal class Program
    {
        static void Function(string path, ref string combinedText)
        {
            try
            {
                string text = File.ReadAllText(path);

                combinedText += text + "\n";
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        static void Main(string[] args)
        {
            string pathOne = "File1.txt";
            string pathTwo = "File2.txt";
            string combinedText = "";

            Thread threadOne = new Thread(() => Function(pathOne, ref combinedText));
            threadOne.Start();
            threadOne.Join();

            Thread threadTwo = new Thread(() => Function(pathTwo, ref combinedText));
            threadTwo.Start();
            threadTwo.Join();

            File.WriteAllText("File3.txt", combinedText);
        }
    }
}
